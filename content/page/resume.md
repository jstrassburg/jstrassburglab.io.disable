---
title: Résumé
subtitle: James Strassburg - Senior Architect
tags: ["résumé"]
comments: false
---

James Strassburg is an experienced software engineer, architect, researcher, and speaker. He has been building distributed software systems and web applications for over 20 years. He is an automation junkie and has worked in cloud technologies, search, artificial intelligence, and devops and has spoken on several related topics.

* I yearn to solve interesting, difficult, and important problems.
* I hope to work with people smarter than myself.
* I have little patience for organizational bureaucracy and politicking.

## Speaking Engagements

* 2014 Lucene Solr Revolution- Washington DC
  * [Evolving Search Relevancy](https://www.youtube.com/watch?v=RHKqcs1Gv3Q&index=20&list=PLU6n9Voqu_1FM8nmVwiWWDRtsEjlPqhgP) - Using genetic algorithms to tune search engine relevancy
* 2015 Direct Supply Technology Symposium - Milwaukee, WI
  * Coupling Deep Dive - An exploration of different forms of software coupling
* 2016 Lucene Solr Revolution - Boston, MA
  * [Building a Solr Continuous Delivery Pipeline with Jenkins](https://www.youtube.com/watch?v=OR8BQl1UfMg&list=PLU6n9Voqu_1F1Skr8qlaO8_VtF4D8JEhm&index=33)
* 2016 Milwaukee School of Engineering Board of Regents
  * Computer Security Presentation
* 2017 Direct Supply Technology Symposium - Milwaukee, WI
  * What Came First, the Infrastructure or the Code? - Using Chef, Packer, and Terraform to do infrastructure as code

Member - Toastmasters since 2016

## Contact Information

<address align="center">
<a href="mailto:jstrassburg@gmail.com">James Strassburg</a><br/>
N111W16484 Esquire Ct<br/>
Germantown, WI  53022 USA<br/>
</address>